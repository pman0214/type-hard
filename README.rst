.. -*- coding: utf-8; -*-

================
 タイプ・ハード
================

タイピング練習、タイピング試験用のWebタイピングソフト。

システム要件
============

* HTTPサーバ

  * ユーザ名を取得するため、サーバの認証（Basic認証など）でページを表示させること。

* PHP

  * SQLiteモジュールが有効化されていること。

セットアップ
============

``db`` ディレクトリはHTTPサーバ実行ユーザが書き込みできるようにしておくこと。

.. code-block:: bash

   % chmod 777 db

結果は ``db/score.db`` に書き込まれる。
後で削除できなくなると困るので、自分がオーナーで作成しておくと良い。

.. code-block:: bash

   % touch db/score.db
   % chmod 666 db/score.db

問題バンク
==========

問題バンクは :code:`JSON` 形式で記述した :code:`problems.json` で与える。
適度にスペースを入れると、問題で表示されるローマ字にもスペースが入る。スペースは全角でも良い。

.. sourcecode:: json

   {
       "problem": [
           {
               "kana": "せいこうとうてい ふゆがたの きあつはいち",
               "text": "西高東低 冬型の気圧配置"
           },
           {
               "kana": "われわれは うちゅうじんだ",
               "text": "我々は宇宙人だ"
           }
       ]
   }

なお、デフォルトで存在している問題バンクの一部は著名人の名言を利用している。

複数の問題ファイルを用意したい場合は、 :code:`problems-xxx.json` などというファイル名にする。
:code:`index.php` を呼び出す際のGETパラメータで :code:`problem=xxx` とすることで問題ファイルを指定できる。

Copyright, License
==================

Copyright (c) 2015-2016, Shigemi ISHIDA

This software is released under the BSD 3-clause license. See LICENSE.
