<?php
/*
 * Copyright (c) 2016, Shigemi ISHIDA
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
?>
<?php
date_default_timezone_set("Asia/Tokyo");
$db_file = 'db/score.db';

$uid     = $_POST['uid'];
$score   = $_POST['score'];
$problem = $_POST['problem'];
$max_backoff = $_POST['max_delay'];

if (!$uid) {
  die('{"ret": -1, "msg": "No result given."}');
}

/* DBファイルを開く */
try {
  $db = new PDO('sqlite:' . $db_file);
} catch (Exception $ex) {
  die('{"ret": -2, "msg": "' . $ex->getMessage() . '"}');
}

/* ランダムバックオフ（0〜max_backoffミリ秒） */
$backoff = mt_rand(0, $max_backoff);
usleep($backoff*1000);

/* テーブルが存在しない場合には作成 */
$ret = $db->beginTransaction();
if ( !$ret ) {
    $er = $db->errorInfo();
    $db = null;
    die('{"ret": -3, "msg": "' . $er[2] . '"}');
}
$ret = $db->exec("CREATE TABLE IF NOT EXISTS type_score (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        uid VARCHAR(16) NOT NULL,
        score INTEGER NOT NULL,
        problem VARCHAR(128) NOT NULL,
        on_create TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
    )");
if ( $ret === False ) {
    $er = $db->errorInfo();
    $db->rollback();
    $db = null;
    die('{"ret": -4, "msg": "' . $er[2] . '"}');
}
$ret = $db->commit();
if ( !$ret ) {
    $er = $db->errorInfo();
    $db->rollback();
    $db = null;
    die('{"ret": -5, "msg": "' . $er[2] . '"}');
}

/* 結果を書き込み */
$ret = $db->beginTransaction();
if ( !$ret ) {
    $er = $db->errorInfo();
    $db = null;
    die('{"ret": -6, "msg": "' . $er[2] . '"}');
}
$ret = $db->exec('INSERT INTO type_score (uid, score, problem) VALUES ("' .
  $uid . '", ' . $score . ', "' . $problem . '")');
if ( $ret === False ) {
    $er = $db->errorInfo();
    $db->rollback();
    $db = null;
    die('{"ret": -7, "msg": "' . $er[2] . '"}');
}
$ret = $db->commit();
if ( !$ret ) {
    $er = $db->errorInfo();
    $db->rollback();
    $db = null;
    die('{"ret": -8, "msg": "' . $er[2] . '"}');
}

echo '{"ret": 0, "msg": "OK", "backoff": ' . $backoff . '}';
$db = null;
