<?php
/*
 * Copyright (c) 2015, Shigemi ISHIDA
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
?>
<?php
/* 成績評価から除外するユーザ */
$exclude_users = ['g-ishida'];

date_default_timezone_set("Asia/Tokyo");

function json_safe_encode($data) {
    return json_encode($data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT);
}

function convert_row($row) {
    /* 点数はintに変換 */
    $row['score'] = intval($row['score']);
    /* 問題ファイルは問題を示す日本語に変換 */
    if ($row['problem'] == "problems.json") {
        $row['problem'] = "文章";
    }
    else if ($row['problem'] == "problems-word.json") {
        $row['problem'] = "単語";
    }
    /* 日付はDateTime objectにしてタイムゾーンを変換 */
    $row['on_create'] = date_create_from_format("Y-m-d H:i:sP", $row['on_create'] . "+0:00");
    $row['on_create'] = date_timezone_set($row['on_create'], timezone_open("Asia/Tokyo"));

    return $row;
}

/* カウント用関数
 * 20点単位で区切ってカウントする
 */
function score_count($scores) {
    /* 最大値をヒストグラムの区切り数が変わるので
     * 最大値に合わせた長さのarrayを作ってゼロで埋める
     */
    $max_score = max($scores);
    $count = array();
    $count = array_pad($count, floor($max_score/20), 0);

    /* 各要素でカウント値を修正 */
    foreach ($scores as $score) {
        $count[floor($score/20)]++;
    }
    /* ラベルを生成 */
    $labels = array();
    foreach (range(0,  floor($max_score/20)) as $cnt) {
        $start = $cnt * 20;
        $labels[] = strval($start) . "〜" . strval($start+19);
    }
    return(array(
               'labels' => $labels,
               'counts' => $count
               ));
}

/* arrayの各要素を''でエスケープする */
function array_str_escape($str) {
    return preg_replace('/^(.*)$/', '\'${1}\'', $str);
}

/*======================================================================*/
$user = $_SERVER["PHP_AUTH_USER"];
if ($user == "") {
  die("Unknown user: 0x80020100\n");
}

$db_file = 'db/score.db';

/* DBファイルを開く */
try {
  $db = new PDO('sqlite:' . $db_file);
} catch (Exception $ex) {
  die("Cannot access DB: " . $ex->getMessage());
}
/* fetchが連想配列になるようにする */
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

/*======================================================================
 * 自分の結果取得
 *======================================================================*/
$results = array();
/* クエリ文 */
$stmt = $db->query("SELECT score, problem, on_create FROM type_score WHERE uid='" . $user . "' ORDER BY on_create DESC");
foreach ($stmt as $row) {
    $results[] = convert_row($row);
}

/*======================================================================
 * 全体の成績分布ヒストグラム用カウント処理
 *======================================================================*/
$scores = array();
/* クエリ文: 無視するユーザがいる場合には除外する */
if (count($exclude_users) > 0) {
    $ex_users_str = implode(",", array_map("array_str_escape", $exclude_users));
    $stmt = $db->query("SELECT MAX(score) FROM type_score WHERE uid NOT IN (" . $ex_users_str . " ) GROUP BY uid ORDER BY MAX(score)");
}
else {
    $stmt = $db->query("SELECT MAX(score) FROM type_score GROUP BY uid ORDER BY MAX(score)");
}
foreach ($stmt as $row) {
    /* intに変換して格納 */
    $score = intval($row['MAX(score)']);
    $scores[] = $score;
}
$hist = score_count($scores);

$stmt->closeCursor();
$db = null;
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>タイプ・ハード結果</title>
  <meta name="author" content="Shigemi ISHIDA">
  <meta name="copyright" content="(C) 2015-<?php echo date("Y"); ?> Shigemi ISHIDA">
  <link rel="stylesheet" href="css/pure-min.css">
  <link rel="stylesheet" href="css/style.css">
  <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/grids-responsive-old-ie-min.css">
  <![endif]-->
  <!--[if gt IE 8]><!-->
    <link rel="stylesheet" href="css/grids-responsive-min.css">
  <!--<![endif]-->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->
  <script src="js/jquery-1.11.3.min.js"></script>
  <script src="js/highcharts.js"></script>
</head>
<body>
<aside>
<div class="nav"><a href="./">[戻る]</a></div>
</aside>
<main>
<div class="main">
<h1 class="page-title">タイプ・ハード受験結果</h1>
<section>
<div class="section">
<h2>ユーザ<span id="user"><?php echo $user; ?></span>の受験結果</h2>
<div class="pure-g">
  <div class="pure-u-1 pure-u-lg-1-3">
    <table class="pure-table">
      <thead>
        <tr>
          <th>日時</th>
          <th>問題</th>
          <th>点数</th>
        </tr>
      </thead>
      <tbody>
<?php foreach ($results as $result) { ?>
        <tr>
          <td><?php echo date_format($result['on_create'], "Y-m-d H:i:s"); ?></td>
          <td><?php echo $result['problem']; ?></td>
          <td><?php echo $result['score'];  ?></td>
        </tr>
<?php } ?>
      </tbody>
    </table>
  </div>
  <div class="pure-u-1 pure-u-lg-2-3">
    <div id="indiv_plot" class="plots"></div>
  </div>
</div>
</div>
</section>
<section>
<div class="section">
<h2>受講者全体の点数分布</h2>
<p>各人の最高点の分布です。</p>
<div id="summary_plot" class="plots"></div>
</div>
</section>
<!-- .main --></div>
</main>
<footer>
<div class="footer">
  &copy; 2015-<?php echo date("Y"); ?> Shigemi Ishida
</div>
</footer>
</body>
<script src="js/date.js"></script>
<script id="plot_script" src="js/plot_results.js"
        data-results ='<?php echo json_safe_encode($results); ?>'
        data-hist ='<?php echo json_safe_encode($hist); ?>'
        ></script>
</html>
