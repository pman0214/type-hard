<?php
/*
 * Copyright (c) 2015, Shigemi ISHIDA
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
?>
<?php
$user = $_SERVER["PHP_AUTH_USER"];
if ($user == "") {
  die("Unknown user: 0x80010100\n");
}
/* デフォルトの問題バンク */
$problem_file = "problems";
if (isset($_GET['problem'])) {
  $problem_file = $problem_file . "-" . $_GET['problem'];
}
$problem_file = $problem_file . ".json";

date_default_timezone_set("Asia/Tokyo");
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>タイプ・ハード</title>
  <meta name="author" content="Shigemi ISHIDA">
  <meta name="copyright" content="(C) 2015-<?php echo date("Y"); ?> Shigemi ISHIDA">
  <link rel="stylesheet" href="css/pure-min.css">
  <link rel="stylesheet" href="css/style.css">
  <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/grids-responsive-old-ie-min.css">
  <![endif]-->
  <!--[if gt IE 8]><!-->
    <link rel="stylesheet" href="css/grids-responsive-min.css">
  <!--<![endif]-->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->
  <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="js/kana-data.js"></script>
  <script type="text/javascript" src="js/roman-data.js"></script>
  <script type="text/javascript" src="js/type-hard.js"></script>
</head>
<body>
<aside>
<div class="nav"><a href="result.php">[結果]</a></div>
</aside>
<main>
<div class="main">
<h1 class="page-title">タイプ・ハード</h1>
<div class="pure-g">
  <div class="pure-u-1 pure-u-lg-3-4"></div>
  <div class="pure-u-1 pure-u-lg-1-4">
    <div class="status">
      <!-- 設定 -->
      <div class="config">
        <label for="errchk_mode">
          <input id="errchk_mode" type="checkbox"> エラーチェックモード
        </label>
        <div class="hidden" id="problem_file"><?php echo $problem_file; ?></div>
      </div>
      <!-- 結果 -->
      <div class="result">
        <div class="user">ユーザ名: <span id="user"><?php echo $user; ?></span></div>
        <div id="elapsed_time">経過時間: 00.0 秒</div>
        <div id="type_speed"></div>
      </div>
    </div>
  </div>
</div>

<!-- 入力 -->
<div class="pure-g type_input">
  <div class="pure-u-1 pure-u-lg-1-24"></div>
  <div class="pure-u-1 pure-u-lg-22-24">
    <input id="input_kana" type="text">
  </div>
  <div class="pure-u-1 pure-u-lg-1-24"></div>
</div>

<!-- 問題 -->
<div class="pure-g problems">
  <div class="pure-u-1 pure-u-lg-1-24"></div>
  <div class="pure-u-1 pure-u-lg-22-24">
    <table id="problems">
    </table>
  </div>
  <div class="pure-u-1 pure-u-lg-1-24"></div>
</div>

<!-- 指示表示領域 -->
<div id="info">スペースキーを押すとスタートします<br>日本語入力はOFFにして下さい</div>
<div id="ajax_status"></div>
<!-- .main --></div>
</main>
<footer>
<div class="footer">
  &copy; 2015-<?php echo date("Y"); ?> Shigemi Ishida
</div>
</footer>
</body>
</html>
