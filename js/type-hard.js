//
// Copyright (c) 2015, Shigemi ISHIDA
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. Neither the name of the Institute nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE.
//

$( function() {
    // 入力用のテキストボックスにフォーカスを固定する
    $("input#input_kana").focus().blur( function () {
        $("input#input_kana").focus();
    });

    //----------------------------------------------------------------------
    // キー処理
    setInterval(key_handling, 1);
    function key_handling() {
        // active or not
        var isActive = $("div#info").hasClass("active") ? true : false;

        if (!isActive) {
            // スタート
            if ( ($("input#input_kana").val() == " ") ||
                 ($("input#input_kana").val() == "　") ) { // space?
                $("div#info").empty().addClass("active");
                $("div#ajax_status").empty();
                startType();
            }
            $("input#input_kana").val("");
        }
        else {
            // タイピング中のキー処理
            typing($("input#input_kana").val());
        }
    }

    var typeCnt = 0;          // 入力した文字数（総計）
    var probTypeCnt = 0;      // 選択中の問題の入力済み文字数
    var kanaOld = "";         // 直前の入力値
    function typing(key_str) {
        // エラーチェックモード
        var isErrChkMode = $("input#errchk_mode").prop("checked");

        // 選択中の問題を取得
        var prob = $("div.problems table#problems tr").eq(probSel);
        var roman = $(prob).children("td.roman").children("span");
        var kana  = $(prob).children("td.kana").children("span");

        // 問題ローマ字のスペースを削除
        var roman_nonsp = roman.text().replace(/\s+/g, "");
        // 問題かなのスペースを削除
        var kana_nonsp  = kana.text().replace(/\s+/g, "");
        // 入力のスペースを削除
        var typed = key_str.replace(/\s+/g, "");

        //------------------------------
        // ローマ字: 入力は一致？
        // 入力の頭の方はかな変換されているのでローマ字に戻す
        var typedRoman = toRoman(typed);
        //  入力と同じ文字数だけ切り出して比べる
        var typeCnt = typedRoman.length;
        if (typedRoman === roman_nonsp.slice(0, typeCnt)) {
            // 一致部分を「入力済み」に
            for (var i=0; i < roman.text().length; i++) {
                if (i < typeCnt) {
                    roman.eq(i).addClass("typed");
                }
                else {
                    roman.eq(i).removeClass("typed");
                }
            }
        }
        else {
            // エラーチェックモードでは間違った入力は許可しないので前の入力状態に戻す
            if (isErrChkMode) {
                $("input#input_kana").val(kanaOld);
                return;
            }
        }

        // ローマ字をかなに変換
        var typedKana  = roman2hiragana(typed);
        // 変換結果が更新された場合はテキストボックスを更新
        if ( $("input#input_kana").val() !== typedKana ) {
            $("input#input_kana").val(typedKana);
        }

        //------------------------------
        // かな: 入力は一致？
        //  入力と同じ文字数だけ切り出して比べる
        typeCnt = typedKana.length;
        if (typedKana === kana_nonsp.slice(0, typeCnt)) {
            probTypeCnt = typeCnt;
            // 一致部分を「入力済み」に
            for (var i=0; i < kana.text().length; i++) {
                if (i < typeCnt) {
                    kana.eq(i).addClass("typed");
                }
                else {
                    kana.eq(i).removeClass("typed");
                }
            }
        }

        kanaOld = $("input#input_kana").val();

        // 問題終了判定: かなで判定
        if (typedKana ===  kana_nonsp) {
            // 「入力済み」を解除しておく
            roman.each( function() {
                $(this).removeClass("typed");
            });
            kana.each( function() {
                $(this).removeClass("typed");
            });
            // 次の問題へ
            nextProblem();
        }
    }

    //----------------------------------------------------------------------
    // 問題を読み込み
    var problem_file = $("#problem_file").text();
    $.getJSON(problem_file, null, function(data) {
        // 一番最初は空の要素を入れておく（表示位置の整合性のため）
        $("<tr/>").hide()
            .append($("<td/>").addClass("text"))
            .append($("<td/>").addClass("kana"))
            .append($("<td/>").addClass("roman"))
            .appendTo("table#problems");
        // 各問題を<tr>として非表示状態で格納しておく
        $(data.problem).each( function() {
            // ローマ字・かなは各文字を<span>で囲う
            var roman = toRoman(this.kana).replace(/(\S)/g, "<span>$1</span>");
            var kana = this.kana.replace(/(\S)/g, "<span>$1</span>");

            $("<tr/>").hide()
                .append($("<td/>").addClass("text").text(this.text))
                .append($("<td/>").addClass("kana").html(kana))
                .append($("<td/>").addClass("roman").html(roman))
                .appendTo("table#problems")
        });
    });

    //----------------------------------------------------------------------
    // 選択されている問題をハイライトする
    var probSel = 1;            // 選択している問題
    function updateProblemView() {
        var prob = $("table#problems tr");
        if (probSel >= 2) {
            // 選択問題-2: 消す
            prob.eq(probSel-2).hide();
        }
        // 選択問題-1: dimming表示
        prob.eq(probSel-1).removeClass("sel").addClass("dim").show();
        // 選択問題  : 選択表示
        prob.eq(probSel).removeClass("dim").addClass("sel").show();
        // 選択問題+1: dimming表示
        prob.eq(probSel+1).removeClass("sel").addClass("dim").fadeIn("slow");
    }

    //----------------------------------------------------------------------
    // 問題表示を次の問題に更新する
    function nextProblem() {
        // タイプカウントを更新
        typeCnt += probTypeCnt;
        probTypeCnt = 0;
        // 次の問題に移動して表示を更新
        probSel++;
        updateProblemView();
        // 入力をクリア
        $("input#input_kana").val("");
        kanaOld = "";
    }

    //----------------------------------------------------------------------
    // タイピングスタート
    function startType() {
        typeCnt = 0;
        probTypeCnt = 0;
        //----------
        // 問題をランダムに並べ替える

        // 0個目は空要素なので、これを除外してから全問題を取得
        $("table#problems tr").eq(0).remove();
        var problems = $("table#problems tr");

        // 並べ替え
        problems = $("table#problems tr");
        problems.sort( function() {
            return(Math.random() - 0.5);
        });

        // 一度全部消してから追加していく（先頭には空要素を追加する）
        $("table#problems").empty();
        $("<tr/>").hide()
            .append($("<td/>").addClass("text"))
            .append($("<td/>").addClass("kana"))
            .append($("<td/>").addClass("roman"))
            .appendTo("table#problems");
        problems.each( function() {
            $(this).removeClass("dim").removeClass("sel")
                .hide().appendTo("table#problems");
        });

        //----------
        // 最初の問題を表示
        probSel = 1;
        updateProblemView();

        //----------
        // タイマをスタート

        // タイマ表示は動作状態にする
        $("div.result div#elapsed_time").removeClass("done");
        // スタート時刻
        startTime = +new Date();
        timer = setInterval(checkTimeLimit, 10);
    }

    //----------------------------------------------------------------------
    // タイピング終了
    function stopType() {
        // 時間の更新を停止
        clearInterval(timer);
        $("div.result div#elapsed_time").addClass("done");
        $("div#info").empty().removeClass("active");
        $("div#info").html("終了！ スペースキーを押すとスタートします<br>日本語入力はOFFにして下さい");

        // 選択中の問題の入力済み状態を解除する
        var prob = $("div.problems table#problems tr").eq(probSel);
        $(prob).children("td.roman").children("span").each( function() {
            $(this).removeClass("typed");
        });
        $(prob).children("td.kana").children("span").each( function() {
            $(this).removeClass("typed");
        });

        // 結果を送信
        storeResult();
    }

    //----------------------------------------------------------------------
    // タイマ・結果表示更新
    function updateTime(elapsed) {
        var timeStr = ("00"+Math.floor(elapsed/1000)).slice(-2) + "."
            + ("0" + Math.floor( (elapsed%1000) / 100 )).slice(-1);
        $("div.result div#elapsed_time").html("経過時間: " + timeStr + " 秒");

        // タイピング結果表示を更新
        var typeSpeed = Math.floor((typeCnt+probTypeCnt) * 60000 / elapsed);
        $("div.result div#type_speed").text("入力速度: " + typeSpeed + " 文字／分");
    }

    // 制限時間チェック
    var timer;
    var startTime = +new Date();
    function checkTimeLimit() {
        // タイマを更新
        var curTime = +new Date();
        var timeInner = curTime - startTime;
        // 60秒を超えたら停止
        if (timeInner >= 60000) {
            stopType();
            timeInner = 60000;
        }
        updateTime(timeInner);
    }

    // 結果保存
    $("div#ajax_status").on('click', 'button', storeResult);
    function storeResult() {
        $("div#ajax_status").html("結果保存中．．．");
        // 最終結果を算出
        var uid = $("span#user").text();
        var score = Math.floor((typeCnt+probTypeCnt));
        $.ajax({url: "send-result.php",
                type: 'POST',
                dataType: 'json',
                data: {'uid': uid, 'score': score, 'problem': problem_file, 'max_delay': 3000},
                timeout: 5000,
                success: function(data) {
                    console.log(data);
                    if (data.ret != 0) {
                        var msg = "<strong>結果保存エラー: " + data.msg + "</strong>";
                        msg += '<p><button class="pure-button button-error" id="resend">結果を再送信</button><p>';
                        $("div#ajax_status").html(msg);
                    }
                    else {
                        $("div#ajax_status").append("完了");
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    var msg = "<strong>結果保存エラー: 通信エラー</strong>";
                    msg += '<p><button class="pure-button button-error" id="resend">結果を再送信</button><p>';
                    $("div#ajax_status").html(msg);
                }
        });
    }
});
