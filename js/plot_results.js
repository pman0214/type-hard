//
// Copyright (c) 2016, Shigemi ISHIDA
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. Neither the name of the Institute nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE.
//

$( function() {
    var $script = $('#plot_script');
    var results = JSON.parse($script.attr('data-results'));
    var hist    = JSON.parse($script.attr('data-hist'));

    // プロットデータ格納用
    var plot_data = [];
    for (var i=results.length-1; i>=0; i--) {
        // 日付を解析
        var d = comDateParse(results[i]['on_create']['date'], 'yyyy-MM-dd HH:mm:ss.fff');
        // グラフはUTCに変換されて描かれるため、9時間進める
        plot_data.push([d.getTime()+60*60*9000, results[i]['score']]);
    }

    $('#summary_plot').highcharts({
        title: {text: '全体の点数分布'},
        xAxis: {
            title: {text: null},
            categories: hist['labels'],
        },
        yAxis: [{
            title: {text: '人数'},
        }],
        series: [{
            name: '点数分布',
            color: 'blue',
            type: 'column',
            data: hist['counts'],
        }],
    });

    $('#indiv_plot').highcharts({
        title: {text: '点数の推移'},
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                millisecond: '%H:%M',
                second: '%H:%M',
                minute: '%H:%M',
                hour: '%m/%d',
                day: '%m/%d',
                week: '%m/%d',
                month: '%Y/%m',
                year: '(%Y)'
            },
            title: {text: null} // 横軸のタイトル
        },
        yAxis: [{ // Primary yAxisの定義
            title: {text: null, style: {color: '#7D2123'}},
            labels: {format: '点',style: {color: '#7D2123'}},
            opposite: false, //右側の軸
        },{ // Secondary yAxisの定義(省略)
            title: {text: null}
        }],
        tooltip: {
            formatter: function() {
                return Highcharts.dateFormat('%m/%d %H:%M', this.x)
                    + ' ' + this.y + '点';
            },
        },
        series: [{
            name: '点数',
            color: 'blue',
            type: 'scatter',
            yAxis: 1,
            turboThreshold: 0,  // 最大データ数は無限大
            data: plot_data,
            tooltip: {
                valueSuffix: '点',
            },
        }],
    });
});
