//
// Originally written by babu_babu_baboo:
//   http://d.hatena.ne.jp/babu_babu_baboo/20091114/1258161477
//
// Modified by Shigemi ISHIDA
//
// Copyright (c) 2015, Shigemi ISHIDA
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. Neither the name of the Institute nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE.
//

var toRoman = (function( ) {
    var roman = {
        '！':'!', '”':'"', '＃':'#', '＄':'$', '％':'%',
        '＆':'&', '’':"'", '（':'(', '）':')', '＝':'=',
        '～':'~', '｜':'|', '＠':'@', '‘':'`', '＋':'+',
        '＊':'*', '；':";", '：':':', '＜':'<', '＞':'>',
        '、':',', '。':'.', '？':'?', '＿':'_',
        '・':'/', '「':'[', '」':']', '｛':'{', '｝':'}',
        '￥':'\\', '＾':'^',

        'ふぁ':'fa', 'ふぃ':'fi', 'ふぇ':'fe', 'ふぉ':'fo',
        'ゔぁ':'vi', 'ゔぃ':'vi', 'ゔぇ':'ve', 'ゔぉ':'vo',

        'きゃ':'kya', 'きゅ':'kyu', 'きょ':'kyo',
        'しゃ':'sha', 'しゅ':'shu', 'しょ':'sho',
        'ちゃ':'tya', 'ちゅ':'tyu', 'ちょ':'tyo',
        'にゃ':'nya', 'にゅ':'nyu', 'にょ':'nyo',
        'ひゃ':'hya', 'ひゅ':'hyu', 'ひょ':'hyo',
        'みゃ':'mya', 'みゅ':'myu', 'みょ':'myo',
        'りゃ':'rya', 'りゅ':'ryu', 'りょ':'ryo',

        'ふゃ':'fya', 'ふゅ':'fyu', 'ふょ':'fyo',
        'ぴゃ':'pya', 'ぴゅ':'pyu', 'ぴょ':'pyo', 'ぴぇ':'pye',
        'びゃ':'bya', 'びゅ':'byu', 'びょ':'byo', 'びぇ':'bye',
        'ぢゃ':'dya', 'ぢゅ':'dyu', 'ぢょ':'dyo', 'ぢぇ':'dye',
        'じゃ':'ja',  'じゅ':'ju',  'じょ':'jo',  'じぇ':'je',
        'ぎゃ':'gya', 'ぎゅ':'gyu', 'ぎょ':'gyo', 'ぎぇ':'gye',

        'ぱ':'pa', 'ぴ':'pi', 'ぷ':'pu', 'ぺ':'pe', 'ぽ':'po',
        'ば':'ba', 'び':'bi', 'ぶ':'bu', 'べ':'be', 'ぼ':'bo',
        'だ':'da', 'ぢ':'di', 'づ':'du', 'で':'de', 'ど':'do',
        'ざ':'za', 'じ':'zi', 'ず':'zu', 'ぜ':'ze', 'ぞ':'zo',
        'が':'ga', 'ぎ':'gi', 'ぐ':'gu', 'げ':'ge', 'ご':'go',
        'ゔ':  'vu',

        'わ':'wa', 'ゐ':'wi', 'う':'wu', 'ゑ':'we', 'を':'wo',
        'ら':'ra', 'り':'ri', 'る':'ru', 'れ':'re', 'ろ':'ro',
        'や':'ya',            'ゆ':'yu',            'よ':'yo',
        'ま':'ma', 'み':'mi', 'む':'mu', 'め':'me', 'も':'mo',
        'は':'ha', 'ひ':'hi', 'ふ':'hu', 'へ':'he', 'ほ':'ho',
        'な':'na', 'に':'ni', 'ぬ':'nu', 'ね':'ne', 'の':'no',
        'た':'ta', 'ち':'ti', 'つ':'tu', 'て':'te', 'と':'to',
        'さ':'sa', 'し':'si', 'す':'su', 'せ':'se', 'そ':'so',
        'か':'ka', 'き':'ki', 'く':'ku', 'け':'ke', 'こ':'ko',
        'あ':'a',  'い':'i',  'う':'u',  'え':'e',  'お':'o',
        'ぁ':'la', 'ぃ':'li', 'ぅ':'lu', 'ぇ':'le', 'ぉ':'lo',

        'ヶ':'ke', 'ヵ':'ka',
        'ん':'nn', 'ー':'-', '　':' ', '−':'-'
    };
    var reg_tu  = /っ([bcdfghijklmnopqrstuvwyz])/gm;
    var reg_xtu = /っ/gm;

    return function (str) {
        var pnt = 0;
        var max = str.length;
        var s, r;
        var txt = '';

        while (pnt <= max) {
            if (r = roman[ str.substring( pnt, pnt + 2 ) ]) {
                txt += r;
                pnt += 2;
            } else {
                txt += ( r = roman[ s = str.substring( pnt, pnt + 1 ) ] ) ? r: s;
                pnt += 1;
            }
        }
        txt = txt.replace(reg_tu, '$1$1');
        txt = txt.replace(reg_xtu, 'xtu');
        return txt;
    };
})();
